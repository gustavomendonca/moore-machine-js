(() => {
  'use strict'

  const MooreMachine = require('./MooreMachine')
  const FsmState = require('./FsmState')
  
  let acumulador = 0

  const estadoErro = new FsmState()
  const aguardaMoeda = new FsmState()
  const add1 = new FsmState()
  const add5 = new FsmState()
  const add10 = new FsmState()
  const add25 = new FsmState()
  const add50 = new FsmState()
  const add100 = new FsmState()
  const fim = new FsmState()
  
  const fsm = new MooreMachine(aguardaMoeda)

  estadoErro.addDefaultTransition(aguardaMoeda)  
  estadoErro.setHandler(() => {
    console.log('erro')
    fsm.transit()
  })
  
  aguardaMoeda.addDefaultTransition(estadoErro)
  aguardaMoeda.addTransition('a', add1)
  aguardaMoeda.addTransition('b', add5)
  aguardaMoeda.addTransition('c', add10)
  aguardaMoeda.addTransition('d', add25)
  aguardaMoeda.addTransition('e', add50)
  aguardaMoeda.addTransition('f', add100)
  aguardaMoeda.addTransition('q', fim)
  aguardaMoeda.setHandler(() => {
    console.log(`Insira moeda... [Total: ${acumulador}]`)
  })

  add1.addDefaultTransition(aguardaMoeda)
  add1.setHandler(() => {
    console.log('1 centavo')
    acumulador+=1
    fsm.transit()
  })

  add5.addDefaultTransition(aguardaMoeda)
  add5.setHandler(() => {
    console.log('5 centavos')
    acumulador+=5
    fsm.transit()
  })

  add10.addDefaultTransition(aguardaMoeda)
  add10.setHandler(() => {
    console.log('10 centavos')
    acumulador+=10
    fsm.transit()
  })

  add25.addDefaultTransition(aguardaMoeda)
  add25.setHandler(() => {
    console.log('25 centavos')
    acumulador+=25
    fsm.transit()
  })

  add50.addDefaultTransition(aguardaMoeda)
  add50.setHandler(() => {
    console.log('50 centavos')
    acumulador+=50
    fsm.transit()
  })

  add100.addDefaultTransition(aguardaMoeda)
  add100.setHandler(() => {
    console.log('1 real')
    acumulador+=100
    fsm.transit()
  })

  fim.addDefaultTransition(estadoErro)
  fim.setHandler(() => {
    console.log(`[Total: ${acumulador}]`)
  })
  

  const stdin = process.openStdin();
  
  stdin.addListener("data", function(d) {
      // note:  d is an object, and when converted to a string it will
      // end with a linefeed.  so we (rather crudely) account for that  
      // with toString() and then trim() 
      //console.log("you entered: [" + 
      //    d.toString().trim() + "]");
      fsm.transit(d.toString().trim())
    });
  

})()