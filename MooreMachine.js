(() => {
  'use strict'

  class MooreMachine {
    constructor(initial){
      this.currentState = initial
    }

    setState(initial) {
      this.currentState = initial
    }

    transit(condition) {
      this.currentState = this.currentState.next(condition)
      this.currentState.execute(condition)
      
    }
  }

  module.exports = MooreMachine
})()