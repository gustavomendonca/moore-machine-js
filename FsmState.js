(() => {
  'use strict'

  class FsmState {
    constructor(){
      this.transitions = []
    }

    addDefaultTransition(state){
      this.defaultState = state
    }

    addTransition(condition, state){
      this.transitions.push({condition: condition, state: state})
    }

    addTransitionByRange(lowerCodition, upperCondition, state){
      for(let i=lowerCodition; i<=upperCondition; i++)
        this.transitions.push({condition: i, state: state})
    }

    removeTransition(condition){
      let index = this.transitions.find((t) => t.condition===condition).map((t, i) => i)
      if (index > -1) this.transitions.splice(index, 1)
    }

    setHandler(handler){
      this.handler = handler
    }

    execute(){
      if (this.handler) this.handler()
    }

    next(condition){
      let transition = this.transitions.find((t) => t.condition===condition)
      return (transition) ? transition.state : this.defaultState
    }
  }

  module.exports = FsmState
})()